import speech_recognition as sr

ENERGY_THRESHOLD = 2000  # Minimum energy for speech. Noises and words below this level will not be recognised
PAUSE_THRESHOLD = 2  # Maximum pause time. After this time the sentence will be parsed by the stt service.
LANG = "it-IT"  # Speech recognition language


def recognize():
    with sr.Microphone() as source:
        r = sr.Recognizer()
        r.energy_threshold = ENERGY_THRESHOLD
        r.pause_threshold = PAUSE_THRESHOLD
        print("Say something!")
        try:
            audio = r.listen(source, PAUSE_THRESHOLD)
            try:
                transcript = r.recognize_google(audio, language=LANG)
                print("You said: " + transcript)
                return transcript
            except sr.UnknownValueError:
                message = "Could not understand audio"
                print(message)
                return '[REPEAT_PLEASE]'
            except:
                print('*** UNDEFINED ERROR ***')
        except sr.WaitTimeoutError:
            print ('*** wait timeout error')
            return '[STT_TIMEOUT]'


if __name__ == "__main__":
    recognize()
